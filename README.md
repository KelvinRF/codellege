# Portafolio Codellege

## Que hago?
1. Crear la carpeta "public".
2. Añadir un "index.html" en la capeta "public".
  - La idea es que sea la pagina principal y tenga una lista de links a todos
  los trabajos que hemos hecho.
3. Guardar y enviar los cambios a GitLab.
Abre una terminal en tu carpeta y sigue los siguientes comandos:
  - git add <nombre de tus archivos>
  - git add README.md public/index.html .gitlab-ci.yml
  - git commit -m "Añadir pagina inicial"
  - git push
4. Añadir una carpeta por cada proyecto o ejercicio que hemos hecho.

## Como reviso?
1. Copia la URL de tu proyecto.
https://gitlab.com/kelvinrangel/plain-html/
2. Convierte la URL de tu proyecto.
https://kelvinrangel.gitlab.io/codellege/
